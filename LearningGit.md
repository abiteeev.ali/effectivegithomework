# Git init

1. Создать локальный git репозиторий
git init
2. Добавить gitignore:
touch .gitignore
3. Добавить Readme:
touch README.md
4. Закоммитить
git commit -m "Initial commit"
5. Обновить remote репозиторий
git remote add origin git@gitlab.com:abiteeev.ali/effectivegithomework.git
git push --set-upstream origin master

## Branches

1. Создать файл LearningGit.md
В него нужно заносить каждый пункт задачи и соответствующее решение
    1. Закомитить
    git add LearningGit.md
    git commit -m "Added file with steps of task progress"
    2. Запушить
    git push
2. Создать ветку main на основе master
3. Создать ветку dev на основе main
4. Удалить ветку master
5. Перенести изменения в remote репозиторий
6. Переименовать dev в develop
7. Обновить remote репозиторий
8. Сделать ветку experiment/git-merge
9. Изменить удалить первую строчку README.md
10. Закомитить
11. Добавить новую строчку в конец файла // контент произвольный
12. Закоммитить
13. Запушить
14. Переключиться на ветку develop
15. Изменить первую строчку README.md
16. Добавить новую строку в в конец файла README.md// контент произвольный
17. Закоммитить и запушить
18. Сделать Pull Request(Merge Request) из ветки experiment/git-merge в develop
19. Локально смержить ветку develop в experiment/git-merge и разрешить конфликты
20. Закоммитить и запушить
21. Убедиться, что нет конфликтов
22. Смержить Merge Request
